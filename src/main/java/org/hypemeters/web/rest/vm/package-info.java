/**
 * View Models used by Spring MVC REST controllers.
 */
package org.hypemeters.web.rest.vm;
